import PropTypes from 'prop-types';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import styles from './HistoryItem.module.scss';
import { removeVideoFromHistory, setActiveVideo } from '../../actions/actions';

// eslint-disable-next-line no-unused-vars
function HistoryItem({ id, title, image }) {
  const dispatch = useDispatch();
  const handleSelect = useCallback(() => dispatch(setActiveVideo(id)), [id]);
  const handleRemove = useCallback(() => dispatch(removeVideoFromHistory(id)), [
    id,
  ]);
  return (
    <div className={styles.container}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <div role="listitem" onClick={handleSelect} className={styles.content}>
        <div className={styles.image}>
          <img alt={title} src={image.url} />
        </div>
        <div className={styles.title}>{title}</div>
      </div>
      {/* eslint-disable-next-line jsx-a11y/interactive-supports-focus */}
      <div role="button" className={styles.delete} onClick={handleRemove}>
        Delete
      </div>
    </div>
  );
}

HistoryItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.shape({
    url: PropTypes.string.isRequired,
  }).isRequired,
};

export default HistoryItem;
