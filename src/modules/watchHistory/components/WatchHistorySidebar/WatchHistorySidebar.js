import React from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { getHistory } from '../../store/watchHistorySelectors';
import HistoryItem from '../HistoryItem';
import styles from './WatchHistorySidebar.module.scss';

function WatchHistorySidebar({ className }) {
  const historyItems = useSelector(getHistory);

  return (
    <div className={cx(className, styles.body)}>
      <div className={styles.history}>View history</div>
      <div className={styles.list}>
        {historyItems.map(item => (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <HistoryItem key={item.id} {...item} />
        ))}
      </div>
    </div>
  );
}

WatchHistorySidebar.defaultProps = {
  className: '',
};

WatchHistorySidebar.propTypes = {
  className: PropTypes.string,
};

export default WatchHistorySidebar;
