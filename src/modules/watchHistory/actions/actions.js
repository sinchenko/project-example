import createAction from '../../core/store/createAction';
import {
  SET_ACTIVE_VIDEO,
  REMOVE_VIDEO_FROM_HISTORY,
  PUT_VIDEO_TO_HISTORY,
} from '../constants/watchHistoryConst';

export const setActiveVideo = createAction(SET_ACTIVE_VIDEO);
export const putVideoToHistory = createAction(PUT_VIDEO_TO_HISTORY);
export const removeVideoFromHistory = createAction(REMOVE_VIDEO_FROM_HISTORY);
