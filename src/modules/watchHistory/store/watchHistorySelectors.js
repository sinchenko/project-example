import { createSelector } from 'reselect';
import { get as getFp } from 'lodash/fp';
import { WATCH_HISTORY_REDUCER_PATH } from './watchHistoryReducer';

export const getHistory = createSelector(
  getFp(WATCH_HISTORY_REDUCER_PATH),
  getFp('history'),
);

export const getActiveVideoID = createSelector(
  getFp(WATCH_HISTORY_REDUCER_PATH),
  getFp('activeVideoID'),
);
