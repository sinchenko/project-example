import update from 'immutability-helper';
import { handleActions } from 'redux-actions';
import createActionType from '../../core/store/createActionType';
import {
  REMOVE_VIDEO_FROM_HISTORY,
  SET_ACTIVE_VIDEO,
  PUT_VIDEO_TO_HISTORY,
} from '../constants/watchHistoryConst';

export const WATCH_HISTORY_REDUCER_PATH = 'watchHistory';

const initialState = {
  activeVideoID: null,
  history: [],
};

export const watchHistoryReducer = handleActions(
  {
    [createActionType(REMOVE_VIDEO_FROM_HISTORY)]: (state, { payload }) => {
      const indexToRemove = state.history.findIndex(({ id }) => id === payload);
      if (indexToRemove < 0) {
        return state;
      }

      const nearestItem =
        state.history[indexToRemove - 1] || state.history[indexToRemove + 1];

      return update(state, {
        activeVideoID: { $set: nearestItem ? nearestItem.id : null },
        history: { $splice: [[indexToRemove, 1]] },
      });
    },
    [createActionType(PUT_VIDEO_TO_HISTORY)]: (state, { payload }) => {
      // Checking for uniq items
      if (state.history.some(({ id }) => id === payload.id)) {
        return state;
      }

      return update(state, {
        history: { $unshift: [payload] },
      });
    },
    [createActionType(SET_ACTIVE_VIDEO)]: (state, { payload }) =>
      update(state, {
        activeVideoID: { $set: payload },
      }),
  },
  initialState,
);
