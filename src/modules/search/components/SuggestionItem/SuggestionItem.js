import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import {
  putVideoToHistory,
  setActiveVideo,
} from '../../../watchHistory/actions/actions';

import styles from './SuggestionItem.module.scss';

function SuggestionItem({ suggestion }) {
  const { image, title, id, description } = suggestion;
  const dispatch = useDispatch();
  const handleClick = useCallback(() => {
    dispatch(putVideoToHistory(suggestion));
    dispatch(setActiveVideo(id));
  }, [dispatch]);

  return (
    <div role="navigation" onClick={handleClick} className={styles.body}>
      <div className={styles.image}>
        <img alt={title} src={image.url} />
      </div>
      <div className={styles.text}>
        <div className={styles.title}>{title}</div>
        <div className={styles.description}>{description}</div>
      </div>
    </div>
  );
}

SuggestionItem.propTypes = {
  suggestion: PropTypes.object.isRequired,
};

export default SuggestionItem;
