import React, { useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { get as getFp } from 'lodash/fp';
import { debounce } from 'lodash';
import cx from 'classnames';
import Autosuggest from 'react-autosuggest';
import SuggestionItem from '../SuggestionItem';
import getVideoList from '../../api/getVideoList';
import { SEARCH_DELAY } from '../../constants/apiCont';

import suggestTheme from './suggestTheme.module.scss';
import styles from './AsyncVideoSuggester.module.scss';

function AsyncVideoSuggester({ className }) {
  const [suggestions, setSuggestions] = useState([]);
  const [searchPhrase, setSearchPhrase] = useState('');

  const onSuggestionsFetchRequested = useCallback(
    debounce(async ({ value }) => {
      const videoList = await getVideoList(value);
      setSuggestions(videoList);
    }, SEARCH_DELAY),
    [setSuggestions], // Actually stay same each render @sinslav
  );

  const onSuggestionsClearRequested = useCallback(() => {
    setSuggestions([]);
  }, [setSuggestions]); // Actually stay same each render @sinslav

  const inputProps = useMemo(
    () => ({
      placeholder: 'Start typing for video search',
      value: searchPhrase,
      onChange: (e, val) => setSearchPhrase(val.newValue),
    }),
    [searchPhrase],
  );

  return (
    <Autosuggest
      onSuggestionsFetchRequested={onSuggestionsFetchRequested}
      onSuggestionsClearRequested={onSuggestionsClearRequested}
      suggestions={suggestions}
      getSuggestionValue={getFp('title')}
      className={cx(className, styles.body)}
      theme={suggestTheme}
      renderSuggestion={suggestion => (
        <SuggestionItem suggestion={suggestion} />
      )}
      inputProps={inputProps}
    />
  );
}

AsyncVideoSuggester.defaultProps = {
  className: '',
};

AsyncVideoSuggester.propTypes = {
  className: PropTypes.string,
};

export default AsyncVideoSuggester;
