import axios from 'axios';
import suggestItemMapperFromApi from './dataMapper/suggestItemMapperFromApi';
import {
  YOUTUBE_API_KEY,
  YOUTUBE_SPI_SEARCH_V3_URL,
} from '../constants/apiCont';

export default searchPhrase =>
  axios
    .get(YOUTUBE_SPI_SEARCH_V3_URL, {
      params: {
        key: YOUTUBE_API_KEY,
        q: searchPhrase,
        part: 'snippet',
        maxResults: 5, // @TODO move to params
        type: 'video',
        videoEmbeddable: true,
      },
    })
    .then(({ data }) => data.items.map(suggestItemMapperFromApi))
    .catch(error => Promise.reject(error)); // @TODO provide error handling
