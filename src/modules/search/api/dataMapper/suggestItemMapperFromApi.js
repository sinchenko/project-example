export default ({ id, snippet: { title, description, thumbnails = {} } }) => ({
  title,
  description,
  image: thumbnails.default,
  id: id.videoId,
});

// TODO provide some data normalization here @sinslav
