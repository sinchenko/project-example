import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { save, load } from 'redux-localstorage-simple';
import {
  WATCH_HISTORY_REDUCER_PATH,
  watchHistoryReducer,
} from '../../watchHistory/store/watchHistoryReducer';

const createStoreWithMiddleware = applyMiddleware(save())(createStore);

const initStore = () =>
  createStoreWithMiddleware(
    combineReducers({ [WATCH_HISTORY_REDUCER_PATH]: watchHistoryReducer }),
    load(),
    composeWithDevTools(),
  );

export default initStore;
