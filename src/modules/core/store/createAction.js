import { createAction } from 'redux-actions';
import createActionType from './createActionType';

export default (name, ...rest) => createAction(createActionType(name), ...rest);
