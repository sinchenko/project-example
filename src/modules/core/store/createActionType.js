import { APP_NAME_PART } from '../const/coreConst';

export default type => `${APP_NAME_PART}${type}`;
