import React, { useEffect } from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import { getActiveVideoID } from '../../../watchHistory/store/watchHistorySelectors';
import useScript from '../../../core/hooks/useScript';

import styles from './VideoPlayer.module.scss';
import { YOUTUBE_API_SRC, YOUTUBE_PLAYER_NODE_ID } from './const';

let playerInstance;

function VideoPlayer() {
  const [loaded] = useScript(YOUTUBE_API_SRC);
  const videoId = useSelector(getActiveVideoID);

  useEffect(() => {
    window.onYouTubePlayerAPIReady = () => {
      // eslint-disable-next-line no-undef
      playerInstance = new YT.Player(YOUTUBE_PLAYER_NODE_ID, {
        videoId,
        height: '100%',
        width: '100%',
      });
    };
    return () => {
      if (playerInstance) {
        playerInstance.destroy();
      }
    };
  }, [loaded]);

  useEffect(() => {
    if (playerInstance) {
      playerInstance.loadVideoById(videoId);
      playerInstance.playVideo();
    }
  }, [videoId]);

  return (
    <>
      <div className={cx(styles.player, { [styles.hidden]: !videoId })}>
        <div id={YOUTUBE_PLAYER_NODE_ID} />
      </div>
      <div className={videoId && styles.hidden}>No video selected</div>
    </>
  );
}

export default VideoPlayer;
