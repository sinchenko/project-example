import React from 'react';
import { CoreLayout, AppHeader, TwoColumnContent } from '../../layout';
import AsyncVideoSuggester from '../../modules/search/components/AsyncVideoSuggester';
import WatchHistorySidebar from '../../modules/watchHistory/components/WatchHistorySidebar';
import VideoPlayer from '../../modules/player/components/VideoPlayer';
import Logo from '../../modules/core/components/Logo';

export default function MainPage() {
  return (
    <CoreLayout>
      <AppHeader>
        <Logo />
        <AsyncVideoSuggester />
      </AppHeader>
      <TwoColumnContent sideBar={WatchHistorySidebar}>
        <VideoPlayer />
      </TwoColumnContent>
    </CoreLayout>
  );
}
