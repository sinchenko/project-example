import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './CoreLayout.module.scss';

const CoreLayout = ({ className, children }) => (
  <div className={cx(styles.body, className)}> {children}</div>
);

CoreLayout.defaultProps = {
  className: '',
};

CoreLayout.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default CoreLayout;
