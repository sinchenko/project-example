import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './TwoColumnContent.module.scss';

const TwoColumnContent = ({ className, children, sideBar: SideBar }) => (
  <div className={cx(styles.wrapper, className)}>
    <SideBar className={styles.sidebar} />
    <div className={styles.content}>{children}</div>
  </div>
);

TwoColumnContent.defaultProps = {
  className: '',
};

TwoColumnContent.propTypes = {
  className: PropTypes.string,
  sideBar: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default TwoColumnContent;
