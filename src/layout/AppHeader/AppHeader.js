import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './AppHeader.module.scss';

function AppHeader({ children, className }) {
  return <header className={cx(className, styles.header)}>{children}</header>;
}

AppHeader.defaultProps = {
  className: '',
};

AppHeader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default AppHeader;
