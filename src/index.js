import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './index.scss';
import App from './App';
import initStore from './modules/core/store/coreReducer';

const store = initStore();

ReactDOM.render(<App store={store} />, document.getElementById('root'));
