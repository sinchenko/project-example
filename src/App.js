import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import MainPage from './pages/MainPage';

function App({ store }) {
  return (
    <Provider store={store}>
      <MainPage />
    </Provider>
  );
}

App.propTypes = {
  store: PropTypes.object.isRequired,
};

export default App;

// @TODO Add Cypress and Jest testing @sinslav
